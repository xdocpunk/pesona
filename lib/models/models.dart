//home===============================================================

class Destinasislider {
  late String id, namaDes, link, alamat;
  Destinasislider(
      {required this.id,
      required this.namaDes,
      required this.link,
      required this.alamat});

  factory Destinasislider.create(Map<String, dynamic> object) {
    return Destinasislider(
        id: object['id'].toString(),
        namaDes: object['nama_destinasi'],
        link: "http://pesona.ukipacu.site/gambar/" +
            object['link_image_destinasi'],
        alamat: object['alamat_destinasi']);
  }
}

class Umkmslider {
  late String id, namaumkm, link, produk;
  Umkmslider(
      {required this.id,
      required this.namaumkm,
      required this.link,
      required this.produk});
  factory Umkmslider.create(Map<String, dynamic> object) {
    return Umkmslider(
        id: object['id'].toString(),
        namaumkm: object['nama_umkm'],
        link: "http://pesona.ukipacu.site/gambar/" + object['link_gambar_umkm'],
        produk: object['produk']);
  }
}

//======================================

class ModelHome {
  late List<String> listCaroselImage;
  late List<Destinasislider> listCaroselDestinasi;
  late List<Umkmslider> listCaroselUmkm;
  late String linkVideo;

  ModelHome(
      {required this.listCaroselImage,
      required this.listCaroselDestinasi,
      required this.listCaroselUmkm,
      required this.linkVideo});

  factory ModelHome.create(Map<String, dynamic> object) {
    List<dynamic> listDestinasiSlider = (object)['data_slider_destinasi'];
    List<Destinasislider> dataDestinasiSlider = [];
    for (int i = 0; i < listDestinasiSlider.length; i++) {
      dataDestinasiSlider.add(Destinasislider.create(listDestinasiSlider[i]));
    }
    //==============================
    List<dynamic> listUmkmSlider = (object)['data_slider_umkm'];
    List<Umkmslider> dataUmkmSlider = [];
    for (int i = 0; i < listUmkmSlider.length; i++) {
      dataUmkmSlider.add(Umkmslider.create(listUmkmSlider[i]));
    }
    //==============================
    List<dynamic> listCaroselImage = (object)['data_slider_image'];
    List<String> dataImageSlider = [];
    for (int i = 0; i < listCaroselImage.length; i++) {
      dataImageSlider
          .add("http://pesona.ukipacu.site/gambar/" + listCaroselImage[i]);
    }
    return ModelHome(
        listCaroselImage: dataImageSlider,
        listCaroselDestinasi: dataDestinasiSlider,
        listCaroselUmkm: dataUmkmSlider,
        linkVideo: object['link_video_youtube']);
  }
}
//page all destinasi,umkm,kategori =====================================

class ModelAll {
  late String namaKatagori, jumlah;
  late List<ItemGridAllScreen> listItem;
  ModelAll(
      {required this.namaKatagori,
      required this.listItem,
      required this.jumlah});

  factory ModelAll.create(Map<String, dynamic> object) {
    List<dynamic> listGridSlider = (object)['data'];
    List<ItemGridAllScreen> dataModelAll = [];
    for (int i = 0; i < listGridSlider.length; i++) {
      dataModelAll.add(ItemGridAllScreen.create(listGridSlider[i]));
    }
    return ModelAll(
        namaKatagori: object['kelompok'],
        listItem: dataModelAll,
        jumlah: object['jumlah_result'].toString());
  }
}

class ItemGridAllScreen {
  late String linkGambar, nama, keterangan, id;
  ItemGridAllScreen(
      {required this.linkGambar,
      required this.nama,
      required this.keterangan,
      required this.id});
  factory ItemGridAllScreen.create(Map<String, dynamic> object) {
    return ItemGridAllScreen(
        linkGambar:
            "http://pesona.ukipacu.site/gambar/" + object['link_gambar'],
        nama: object['nama'],
        keterangan: object['keterangan'],
        id: object['id'].toString());
  }
}

//detail ==============================================================================
class ModelDetail {
  late String id,
      namaDes,
      deskr,
      linkVideo,
      alamat,
      jambuka,
      jamtutup,
      telepon,
      instagrm;
  late double latitude, longtitude;
  late List<String> linkGambar;
  ModelDetail(
      {required this.id,
      required this.namaDes,
      required this.deskr,
      required this.linkVideo,
      required this.alamat,
      required this.jambuka,
      required this.jamtutup,
      required this.telepon,
      required this.instagrm,
      required this.latitude,
      required this.longtitude,
      required this.linkGambar});
  factory ModelDetail.create(Map<String, dynamic> object) {
    List<dynamic> listgambardata = (object)['data_image'];
    List<String> dataImage = [];
    for (int i = 0; i < listgambardata.length; i++) {
      dataImage.add("http://pesona.ukipacu.site/gambar/" + listgambardata[i]);
    }
    return ModelDetail(
        id: object['id'].toString(),
        namaDes: object['nama'],
        deskr: object['deskripsi'],
        linkVideo: object['link_video'],
        alamat: object['alamat'],
        jambuka: object['jam_buka'],
        jamtutup: object['jam_tutup'],
        telepon: object['telp'],
        instagrm: object['insta'],
        latitude: double.parse(object['lat']),
        longtitude: double.parse(object['long']),
        linkGambar: dataImage);
  }
}

//pencarian ============================================================
class ModelSearch {
  late String totalResult, key;
  late List<ItemPencarian> listDataPencarian;
  ModelSearch(
      {required this.key,
      required this.totalResult,
      required this.listDataPencarian});
  factory ModelSearch.create(Map<String, dynamic> object) {
    List<dynamic> listSearch = (object)['data_search'];
    List<ItemPencarian> dataListSearch = [];
    for (int i = 0; i < listSearch.length; i++) {
      dataListSearch.add(ItemPencarian.create(listSearch[i]));
    }
    return ModelSearch(
        key: object['key'],
        totalResult: object['total_result'].toString(),
        listDataPencarian: dataListSearch);
  }
}

class ItemPencarian {
  late String id, link, nama, kategori, deskripsi;
  ItemPencarian(
      {required this.id,
      required this.link,
      required this.nama,
      required this.kategori,
      required this.deskripsi});
  factory ItemPencarian.create(Map<String, dynamic> object) {
    return ItemPencarian(
        id: object['id'].toString(),
        link: "http://pesona.ukipacu.site/gambar/" + object['link_gambar'],
        nama: object['nama'],
        kategori: object['kategori'],
        deskripsi: object['deskripsi']);
  }
}
//ModelKLU==============================================================

class ModelKlu {
  late String deskripsi;
  late List<String> listGambar;
  late List<ItemKecamatanSlider> listItemKecamatan;
  ModelKlu(
      {required this.deskripsi,
      required this.listGambar,
      required this.listItemKecamatan});
  factory ModelKlu.create(Map<String, dynamic> object) {
    //==========================================================================
    List<dynamic> listImage = (object)['data_galery_image'];
    List<String> dataImage = [];
    for (int i = 0; i < listImage.length; i++) {
      dataImage.add("http://pesona.ukipacu.site/gambar/" + listImage[i]);
    }
    //==========================================================================
    List<ItemKecamatanSlider> dataListKecamatan = [
      ItemKecamatanSlider(
          id: 'bayan',
          nama: 'Bayan',
          link: 'images/slider_bayan.jpg',
          alamat: 'alamatnya'),
      ItemKecamatanSlider(
          id: 'gangga',
          nama: 'Gangga',
          link: 'images/slider_gangga.jpg',
          alamat: 'alamatnya'),
      ItemKecamatanSlider(
          id: 'kayangan',
          nama: 'Kayangan',
          link: 'images/slider_kayangan.jpg',
          alamat: 'alamatnya'),
      ItemKecamatanSlider(
          id: 'pemenang',
          nama: 'Pemenang',
          link: 'images/slider_pemenang.jpg',
          alamat: 'alamatnya'),
      ItemKecamatanSlider(
          id: 'tanjung',
          nama: 'Tanjung',
          link: 'images/slider_tanjung.jpg',
          alamat: 'alamatnya'),
    ];
    return ModelKlu(
        deskripsi: object['deskripsi'],
        listGambar: dataImage,
        listItemKecamatan: dataListKecamatan);
  }
}

class ItemKecamatanSlider {
  late String id, nama, link, alamat;
  ItemKecamatanSlider(
      {required this.id,
      required this.nama,
      required this.link,
      required this.alamat});
  factory ItemKecamatanSlider.create(Map<String, dynamic> object) {
    return ItemKecamatanSlider(
        id: object['id'],
        nama: object['nama'],
        link: object['link_gambar'],
        alamat: object['alamat']);
  }
}

//modelKecamatan========================================================
class ModelKecamatan {
  late String linkGambar, nama;
  late List<Kategori> listKategori;
  late List<ItemKecamatanSlider> listKecamatanSlider;
  late List<ItemListViewKecamatan> listViewKecamatan;
  ModelKecamatan(
      {required this.linkGambar,
      required this.nama,
      required this.listKategori,
      required this.listKecamatanSlider,
      required this.listViewKecamatan});
  factory ModelKecamatan.create(Map<String, dynamic> object) {
    //============================================================
    List<dynamic> listKategori = (object)['data_kategori'];
    List<Kategori> dataListKategori = [];
    for (int i = 0; i < listKategori.length; i++) {
      dataListKategori.add(Kategori.create(listKategori[i]));
    }
    //=============================================================
    List<ItemKecamatanSlider> dataListSlider = [
      ItemKecamatanSlider(
          id: 'bayan',
          nama: 'Bayan',
          link: 'images/slider_bayan.jpg',
          alamat: 'alamatnya'),
      ItemKecamatanSlider(
          id: 'gangga',
          nama: 'Gangga',
          link: 'images/slider_gangga.jpg',
          alamat: 'alamatnya'),
      ItemKecamatanSlider(
          id: 'kayangan',
          nama: 'Kayangan',
          link: 'images/slider_kayangan.jpg',
          alamat: 'alamatnya'),
      ItemKecamatanSlider(
          id: 'pemenang',
          nama: 'Pemenang',
          link: 'images/slider_pemenang.jpg',
          alamat: 'alamatnya'),
      ItemKecamatanSlider(
          id: 'tanjung',
          nama: 'Tanjung',
          link: 'images/slider_tanjung.jpg',
          alamat: 'alamatnya'),
    ];

    //=============================================================
    List<dynamic> listViewKecamatan = (object)['data_destinasi'];
    List<ItemListViewKecamatan> dataListViewKecamatan = [];
    for (int i = 0; i < listViewKecamatan.length; i++) {
      dataListViewKecamatan
          .add(ItemListViewKecamatan.create(listViewKecamatan[i]));
    }
    String gambarBeelakang = "images/slider_tanjung.jpg";

    switch (object['kecamatan']) {
      case "bayan":
        gambarBeelakang = "images/slider_bayan.jpg";
        break;
      case "gangga":
        gambarBeelakang = "images/slider_gangga.jpg";
        break;
      case "kayangan":
        gambarBeelakang = "images/slider_kayangan.jpg";
        break;
      case "pemenang":
        gambarBeelakang = "images/slider_pemenang.jpg";
        break;
      case "tanjung":
        gambarBeelakang = "images/slider_tanjung.jpg";
        break;
      default:
        gambarBeelakang = "images/slider_tanjung.jpg";
        break;
    }
    return ModelKecamatan(
        linkGambar: gambarBeelakang,
        nama: object['kecamatan'],
        listKategori: dataListKategori,
        listKecamatanSlider: dataListSlider,
        listViewKecamatan: dataListViewKecamatan);
  }
}

class Kategori {
  late String nama, kode;
  Kategori({required this.nama, required this.kode});
  factory Kategori.create(Map<String, dynamic> object) {
    return Kategori(nama: object['nama_kategori'], kode: object['alias']);
  }
}

class ItemListViewKecamatan {
  late String id, link, nama, deskripsi;
  ItemListViewKecamatan(
      {required this.id,
      required this.link,
      required this.nama,
      required this.deskripsi});
  factory ItemListViewKecamatan.create(Map<String, dynamic> object) {
    return ItemListViewKecamatan(
        id: object['id'].toString(),
        link: "http://pesona.ukipacu.site/gambar/" + object['link_gambar'],
        nama: object['nama'],
        deskripsi: object['deskripsi']);
  }
}

class ListViewKecamatan {
  late List<ItemListViewKecamatan> listdestinasi;
  ListViewKecamatan({required this.listdestinasi});
  factory ListViewKecamatan.create(Map<String, dynamic> object) {
    List<dynamic> listViewKecamatan = (object)['data_destinasi'];
    List<ItemListViewKecamatan> dataListViewKecamatan = [];
    for (int i = 0; i < listViewKecamatan.length; i++) {
      dataListViewKecamatan
          .add(ItemListViewKecamatan.create(listViewKecamatan[i]));
    }
    return ListViewKecamatan(listdestinasi: dataListViewKecamatan);
  }
}

//more========================================================================
// class ModelMore {
//   late String jmlh;
//   late List<String> listGambar;
//   ModelMore({required this.jmlh, required this.listGambar});
//   factory ModelMore.create(Map<String, dynamic> object) {
//     List<dynamic> listImage = (object)['data_image'];
//     List<String> dataImage = [];
//     for (int i = 0; i < listImage.length; i++) {
//       dataImage.add(listImage[i]);
//     }
//     return ModelMore(jmlh: object['jmlh'], listGambar: dataImage);
//   }
//   static Future<ModelMore> moreKluToApi(String key) async {
//     String apiurl = 'https://onepiece.themeforall.my.id/api/more.php';
//     var apiresult = await http.get(Uri.parse(apiurl));
//     var jesonObject = jsonDecode(apiresult.body);
//     return ModelMore.create(jesonObject);
//   }

//   static Future<ModelMore> moreDetailDestinasiToApi(String key) async {
//     String apiurl = 'https://onepiece.themeforall.my.id/api/more.php';
//     var apiresult = await http.get(Uri.parse(apiurl));
//     var jesonObject = jsonDecode(apiresult.body);
//     return ModelMore.create(jesonObject);
//   }
// }
