import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:klu/data/api_provider.dart';
import 'package:klu/data/bloc.dart';
import 'package:klu/models/models.dart';
import 'package:klu/screen/search/widget_search.dart';

class Search extends StatefulWidget {
  final ModelSearch modelSearch;
  const Search({Key? key, required this.modelSearch}) : super(key: key);

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  late ModelSearch m;
  String keySearch = "";
  @override
  void initState() {
    super.initState();
    m = widget.modelSearch;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          backgroundColor: const Color.fromRGBO(12, 110, 25, 1),
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Icon(
            CommunityMaterialIcons.arrow_left_bold_circle_outline,
            color: Colors.white,
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Stack(
            children: [
              //background==================
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.1,
                color: const Color.fromRGBO(12, 110, 25, 1),
              ),
              //content=====================
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                        top: 33.h, left: 20.w, right: 20.w, bottom: 10.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          m.key,
                          style: TextStyle(
                              fontFamily: 'Lato',
                              color: Colors.white,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          '(' + m.totalResult + ' ditemukan)',
                          style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 10.sp,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    margin: EdgeInsets.only(top: 5.h),
                    child: TextField(
                      onSubmitted: (value) {
                        BlocApp _bloce = BlocApp("cari", value);
                        _bloce.cariStream.listen((event) {
                          switch (event.status) {
                            case Status.LOADING:
                              EasyLoading.show(status: 'loading...');
                              break;
                            case Status.COMPLETED:
                              EasyLoading.dismiss();
                              _bloce.dispose("cari");
                              m = event.data;
                              setState(() {});
                              break;
                            case Status.ERROR:
                              EasyLoading.dismiss();
                              EasyLoading.showError(event.message);
                              _bloce.dispose("cari");
                              break;
                          }
                        });
                      },
                      textInputAction: TextInputAction.search,
                      autofocus: false,
                      decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        prefixIcon: Icon(
                          CommunityMaterialIcons.map_search,
                          color: Colors.grey,
                        ),
                        hintText: 'masukan yang dicari',
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                      ),
                    ),
                  ),
                  (m.listDataPencarian.isEmpty)
                      ? Flexible(
                          child: Center(
                              child: Text(
                          "Tidak ada data ditemukan",
                          style: TextStyle(fontFamily: 'Lato', fontSize: 20.sp),
                        )))
                      : Flexible(
                          child: ListPencarian(listItem: m.listDataPencarian))
                ],
              ),
            ],
          ),
        ));
  }
}
