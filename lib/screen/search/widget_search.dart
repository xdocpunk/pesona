import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:klu/data/api_provider.dart';
import 'package:klu/data/bloc.dart';
import 'package:klu/models/models.dart';
import 'package:klu/screen/detail_destinasi/detail.dart';

class ListPencarian extends StatefulWidget {
  final List<ItemPencarian> listItem;

  const ListPencarian({Key? key, required this.listItem}) : super(key: key);

  @override
  _ListPencarianState createState() => _ListPencarianState();
}

class _ListPencarianState extends State<ListPencarian> {
  late ModelDetail modelDetail;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: widget.listItem.length,
      itemBuilder: (context, index) {
        return (widget.listItem[index].kategori != "Berita")
            ? (widget.listItem[index].kategori != "Tentang KLU")
                ? GestureDetector(
                    onTap: () {
                      BlocApp _bloce =
                          BlocApp("detail", (widget.listItem[index].id));
                      _bloce.detailStream.listen((event) {
                        switch (event.status) {
                          case Status.LOADING:
                            EasyLoading.show(status: 'loading...');
                            break;
                          case Status.COMPLETED:
                            EasyLoading.dismiss();
                            _bloce.dispose("detail");
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return Detail(detail: event.data);
                            }));
                            break;
                          case Status.ERROR:
                            EasyLoading.dismiss();
                            EasyLoading.showError(event.message);
                            _bloce.dispose("detail");
                            break;
                        }
                      });
                    },
                    child: ItemListDest(
                        link: widget.listItem[index].link,
                        namaDes: widget.listItem[index].nama,
                        ket: widget.listItem[index].deskripsi,
                        kategori: widget.listItem[index].kategori),
                  )
                : Container()
            : Container();
      },
    );
  }
}

//item list ==============================================================================

class ItemListDest extends StatefulWidget {
  final String link;
  final String namaDes;
  final String ket;
  final String kategori;
  const ItemListDest(
      {Key? key,
      required this.link,
      required this.namaDes,
      required this.ket,
      required this.kategori})
      : super(key: key);

  @override
  _ItemListDestState createState() => _ItemListDestState();
}

class _ItemListDestState extends State<ItemListDest> {
  Map<String, IconData> iconMapping = {
    'Wisata': CommunityMaterialIcons.map_marker_distance,
    'Hotel': CommunityMaterialIcons.home_modern,
    'Bensin': CommunityMaterialIcons.gas_station,
    'Kesehatan': CommunityMaterialIcons.hospital_box,
    'UMKM': CommunityMaterialIcons.circle_box_outline,
    'Event': CommunityMaterialIcons.calendar_text,
    'Makanan': CommunityMaterialIcons.food,
    'DesaWisata': CommunityMaterialIcons.pine_tree,
  };
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10.w, right: 10.w, bottom: 10.h),
      padding: EdgeInsets.all(5.w),
      decoration: const BoxDecoration(
          // border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                color: Colors.grey,
                blurRadius: 5,
                spreadRadius: 1,
                offset: Offset(0, 3))
          ],
          color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 100.w,
            height: 100.w,
            margin: const EdgeInsets.all(5),
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(5)),
                image: DecorationImage(
                    image: NetworkImage(widget.link), fit: BoxFit.fill)),
            // image: DecorationImage(
            //     image: NetworkImage(
            //         "https://blog.reservasi.com/wp-content/uploads/2018/07/shutterstock_1064654387-1.jpg"),
            //     fit: BoxFit.fill)),
          ),
          SizedBox(
            width: 10.w,
          ),
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.namaDes,
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 20.sp,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      overflow: TextOverflow.ellipsis),
                ),
                SizedBox(
                  height: 5.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      iconMapping[widget.kategori],
                      color: Colors.orange,
                      size: 15.h,
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    Text(
                      widget.kategori,
                      style: TextStyle(
                          fontFamily: 'Lato',
                          fontSize: 15.sp,
                          color: Colors.black,
                          overflow: TextOverflow.ellipsis),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      CommunityMaterialIcons.card_text_outline,
                      color: Colors.orange,
                      size: 15.h,
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.only(bottom: 10.h),
                        child: Text(
                          stripHtmlIfNeeded(widget.ket),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 12.sp,
                              color: Colors.black),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

String stripHtmlIfNeeded(String text) {
  return text.replaceAll(RegExp(r'<[^>]*>|&[^;]+;'), ' ');
}
