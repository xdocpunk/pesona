import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:klu/models/models.dart';
import 'widget_list_destinasi.dart';
import 'package:community_material_icon/community_material_icon.dart';

class ListDestinasi extends StatefulWidget {
  final ModelAll modelAll;
  const ListDestinasi({Key? key, required this.modelAll}) : super(key: key);

  @override
  _ListDestinasiState createState() => _ListDestinasiState();
}

class _ListDestinasiState extends State<ListDestinasi> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          backgroundColor: const Color.fromRGBO(12, 110, 25, 1),
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Icon(
            CommunityMaterialIcons.arrow_left_bold_circle_outline,
            color: Colors.white,
          ),
        ),
        body: Stack(
          children: [
            //background==================
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.1,
              color: const Color.fromRGBO(12, 110, 25, 1),
            ),
            //content=====================
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(
                      top: 33.h, left: 20.w, right: 20.w, bottom: 10.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        widget.modelAll.namaKatagori,
                        style: TextStyle(
                            fontFamily: 'Lato',
                            color: Colors.white,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '(' + widget.modelAll.jumlah + ' ditemukan)',
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 10.sp,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Flexible(
                    child: GridDestinasi(
                  listItem: widget.modelAll.listItem,
                ))
              ],
            ),
          ],
        ));
  }
}
