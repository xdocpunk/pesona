import 'package:flutter/material.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:klu/data/api_provider.dart';
import 'package:klu/data/bloc.dart';
import 'package:klu/models/models.dart';
import 'package:klu/screen/detail_destinasi/detail.dart';

class GridDestinasi extends StatefulWidget {
  final List<ItemGridAllScreen> listItem;
  const GridDestinasi({Key? key, required this.listItem}) : super(key: key);

  @override
  _GridDestinasiState createState() => _GridDestinasiState();
}

class _GridDestinasiState extends State<GridDestinasi> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 2,
        childAspectRatio: 8 / 5,
      ),
      shrinkWrap: true,
      itemCount: widget.listItem.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            BlocApp _bloce = BlocApp("detail", widget.listItem[index].id);
            _bloce.detailStream.listen((event) {
              switch (event.status) {
                case Status.LOADING:
                  EasyLoading.show(status: 'loading...');
                  break;
                case Status.COMPLETED:
                  EasyLoading.dismiss();
                  _bloce.dispose("detail");
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return Detail(detail: event.data);
                  }));
                  break;
                case Status.ERROR:
                  EasyLoading.dismiss();
                  EasyLoading.showError(event.message);
                  _bloce.dispose("detail");
                  break;
              }
            });
          },
          child: ItemGridDestinasi(
              linkGambar: widget.listItem[index].linkGambar,
              nama: widget.listItem[index].nama,
              ket: widget.listItem[index].keterangan),
        );
      },
    );
  }
}

//item grid =========================================
class ItemGridDestinasi extends StatelessWidget {
  final String linkGambar;
  final String nama;
  final String ket;
  const ItemGridDestinasi(
      {Key? key,
      required this.linkGambar,
      required this.nama,
      required this.ket})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Container(
        padding: const EdgeInsets.all(5),
        margin: EdgeInsets.only(right: 7.w, left: 8.w, bottom: 7),
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(10),
                topRight: Radius.circular(10),
                topLeft: Radius.circular(10),
                bottomRight: Radius.circular(10)),
            image: DecorationImage(
                image: NetworkImage(linkGambar), fit: BoxFit.fill)
            // image: DecorationImage(
            //     image: NetworkImage(
            //         "https://blog.reservasi.com/wp-content/uploads/2018/07/shutterstock_1064654387-1.jpg"),
            //     fit: BoxFit.fill)
            ),
        width: 20.w,
        height: 10.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              nama,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontSize: 15.sp,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  CommunityMaterialIcons.map_marker,
                  size: 10.w,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    stripHtmlIfNeeded(ket),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 10.sp,
                        color: Colors.white),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

String stripHtmlIfNeeded(String text) {
  return text.replaceAll(RegExp(r'<[^>]*>|&[^;]+;'), ' ');
}
