import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:klu/data/api_provider.dart';
import 'package:klu/data/bloc.dart';
import 'package:klu/models/models.dart';
import 'package:klu/screen/destinasi/list_destinasi.dart';
import 'package:klu/screen/detail_destinasi/detail.dart';
import 'package:klu/screen/klu/about_klu.dart';
import 'package:klu/screen/search/search.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import 'package:marquee/marquee.dart';

//widget bagian atas===================================================================
class Widgetatas extends StatelessWidget {
  final List<String> listGambarCarosel;
  final YoutubePlayerController ytb;
  const Widgetatas(
      {Key? key, required this.listGambarCarosel, required this.ytb})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.38,
      child: Column(
        children: [
          //bagian bawah dan logo
          Stack(
            children: [
              SizedBox(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.3,
                  child: CarouselSlider(
                    options: CarouselOptions(
                        height: double.infinity,
                        autoPlay: true,
                        viewportFraction: 1.0,
                        autoPlayInterval: const Duration(seconds: 3),
                        autoPlayAnimationDuration:
                            const Duration(milliseconds: 800),
                        autoPlayCurve: Curves.fastOutSlowIn),
                    items: listGambarCarosel.map((i) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(right: 1.0),
                              decoration:
                                  const BoxDecoration(color: Colors.grey),
                              child: Image.network(
                                i,
                                fit: BoxFit.fill,
                                alignment: Alignment.center,
                                loadingBuilder: (BuildContext context,
                                    Widget child,
                                    ImageChunkEvent? loadingProgress) {
                                  if (loadingProgress == null) return child;
                                  return Center(
                                    child: CircularProgressIndicator(
                                      value:
                                          loadingProgress.expectedTotalBytes !=
                                                  null
                                              ? loadingProgress
                                                      .cumulativeBytesLoaded /
                                                  loadingProgress
                                                      .expectedTotalBytes!
                                              : null,
                                    ),
                                  );
                                },
                              ));
                        },
                      );
                    }).toList(),
                  )),
              Container(
                  margin: const EdgeInsets.only(left: 10, top: 25),
                  child: Row(
                    children: [
                      Image.asset(
                        'images/logo_klu.png',
                        width: MediaQuery.of(context).size.width * 0.1,
                        height: MediaQuery.of(context).size.width * 0.1,
                      ),
                      Image.asset(
                        'images/logo_kominfo.png',
                        width: MediaQuery.of(context).size.width * 0.1,
                        height: MediaQuery.of(context).size.width * 0.1,
                      )
                    ],
                  )),
              Container(
                  width: double.infinity,
                  height: 50.h,
                  margin: EdgeInsets.only(top: 150.h),
                  child: Marquee(
                    text:
                        'Selamat Datang Di Kabupaten Lombok Utara,Nikmati Liburan mu dengan Mengeksplorasi Destinasi - Destinasi Wisata, Aneka Jajanan, Makanan Khas Melalui aplikasi ini',
                    style: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 17.sp,
                        color: Colors.white),
                    blankSpace: 50,
                  )),
            ],
          ),
          //bagian atas search dan dekripsi
          // SizedBox(
          //   height: 5.h,
          // ),
          // Column(
          //   mainAxisAlignment: MainAxisAlignment.end,
          //   children: [
          //deskripsi nya======================
          // Container(
          //   margin: EdgeInsets.only(left: 10.w, right: 10.w),
          //   padding: const EdgeInsets.all(5),
          //   decoration: BoxDecoration(
          //     border: Border.all(color: Colors.black, width: 1),
          //     borderRadius: BorderRadius.all(Radius.circular(10)),
          //     // boxShadow: [
          //     //   BoxShadow(
          //     //       color: Colors.grey,
          //     //       blurRadius: 5,
          //     //       spreadRadius: 1,
          //     //       offset: Offset(0, 3))
          //     // ]
          //   ),
          //   child: Column(
          //     mainAxisAlignment: MainAxisAlignment.start,
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: [
          //       // Text(
          //       //   'Kabupaten Lombok Utara',
          //       //   style: TextStyle(
          //       //     fontFamily: 'Cadillac',
          //       //     fontSize: 25.sp,
          //       //     color: Colors.red,
          //       //   ),
          //       // ),
          //       // Text(
          //       //   'Selamat Datang Di Kabupaten Lombok Utara,Nikmati Liburan mu dengan Mengeksplorasi Destinasi - Destinasi Wisata, Aneka Jajanan, Makanan Khas Melalui aplikasi ini',
          //       //   overflow: TextOverflow.ellipsis,
          //       //   textAlign: TextAlign.justify,
          //       //   maxLines: 3,
          //       //   style: TextStyle(fontFamily: 'Lato', fontSize: 12.sp),
          //       // )
          //       SizedBox(
          //           width: double.infinity,
          //           height: 50,
          //           child: Marquee(
          //             text:
          //                 'Selamat Datang Di Kabupaten Lombok Utara,Nikmati Liburan mu dengan Mengeksplorasi Destinasi - Destinasi Wisata, Aneka Jajanan, Makanan Khas Melalui aplikasi ini',
          //             style: TextStyle(fontFamily: 'Lato', fontSize: 17.sp),
          //             blankSpace: 50,
          //           )),
          //     ],
          //   ),
          // )
          //   ],
          // ),
          // SizedBox(
          //   height: 10.h,
          // ),
          Container(
            margin: const EdgeInsets.only(left: 10, right: 10),
            width: double.infinity,
            height: 37.h,
            child: Center(
              child: TextField(
                textInputAction: TextInputAction.search,
                onSubmitted: (valuee) {
                  ytb.pause();
                  BlocApp _bloce = BlocApp("cari", valuee);
                  _bloce.cariStream.listen((event) {
                    switch (event.status) {
                      case Status.LOADING:
                        EasyLoading.show(status: 'loading...');
                        break;
                      case Status.COMPLETED:
                        EasyLoading.dismiss();
                        _bloce.dispose("cari");
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return Search(modelSearch: event.data);
                        }));
                        break;
                      case Status.ERROR:
                        EasyLoading.dismiss();
                        EasyLoading.showError(event.message);
                        _bloce.dispose("cari");
                        break;
                    }
                  });
                },
                autofocus: false,
                decoration: const InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    prefixIcon: Icon(
                      CommunityMaterialIcons.map_search,
                      color: Colors.grey,
                    ),
                    hintText: 'masukan Kata yang dicari',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)))),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
//carosel atas===================================================================

// kategori ====================================================

class KategorBawah extends StatefulWidget {
  final YoutubePlayerController ytb;
  const KategorBawah({Key? key, required this.ytb}) : super(key: key);

  @override
  State<KategorBawah> createState() => _KategorBawahState();
}

class _KategorBawahState extends State<KategorBawah> {
  late ModelKlu modelKlu;
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      primary: false,
      padding: const EdgeInsets.only(right: 15, left: 15, top: 15, bottom: 15),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 4,
      shrinkWrap: true,
      children: [
        GestureDetector(
          onTap: () {
            widget.ytb.pause();
            BlocApp _bloce = BlocApp("klu", "");
            _bloce.kluStream.listen((event) {
              switch (event.status) {
                case Status.LOADING:
                  EasyLoading.show(status: 'loading...');
                  break;
                case Status.COMPLETED:
                  EasyLoading.dismiss();
                  _bloce.dispose("klu");
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return Aboutklu(modelKlu: event.data);
                  }));
                  break;
                case Status.ERROR:
                  EasyLoading.dismiss();
                  EasyLoading.showError(event.message);
                  _bloce.dispose("klu");
                  break;
              }
            });
          },
          child: const ButtonKategori(
            judul: 'Tentang KLU',
            iconnya: CommunityMaterialIcons.information_outline,
          ),
        ),
        GestureDetector(
          onTap: () {
            widget.ytb.pause();
            // EasyLoading.show(status: 'loading...');
            BlocApp _bloce = BlocApp("all", "Makanan");
            _bloce.allStream.listen((event) {
              switch (event.status) {
                case Status.LOADING:
                  EasyLoading.show(status: 'loading...');
                  break;
                case Status.COMPLETED:
                  EasyLoading.dismiss();
                  _bloce.dispose("all");
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ListDestinasi(modelAll: event.data);
                  }));
                  break;
                case Status.ERROR:
                  EasyLoading.dismiss();
                  EasyLoading.showError(event.message);
                  _bloce.dispose("all");
                  break;
              }
            });
          },
          child: const ButtonKategori(
            judul: 'Makanan',
            iconnya: CommunityMaterialIcons.food,
          ),
        ),
        GestureDetector(
          onTap: () {
            widget.ytb.pause();
            BlocApp _bloce = BlocApp("all", "wisata");
            _bloce.allStream.listen((event) {
              switch (event.status) {
                case Status.LOADING:
                  EasyLoading.show(status: 'loading...');
                  break;
                case Status.COMPLETED:
                  EasyLoading.dismiss();
                  _bloce.dispose("all");
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ListDestinasi(modelAll: event.data);
                  }));
                  break;
                case Status.ERROR:
                  EasyLoading.dismiss();
                  EasyLoading.showError(event.message);
                  _bloce.dispose("all");
                  break;
              }
            });
          },
          child: const ButtonKategori(
            judul: 'Objek Wisata',
            iconnya: CommunityMaterialIcons.map_marker_distance,
          ),
        ),
        GestureDetector(
          onTap: () {
            widget.ytb.pause();
            BlocApp _bloce = BlocApp("all", "Hotel");
            _bloce.allStream.listen((event) {
              switch (event.status) {
                case Status.LOADING:
                  EasyLoading.show(status: 'loading...');
                  break;
                case Status.COMPLETED:
                  EasyLoading.dismiss();
                  _bloce.dispose("all");
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ListDestinasi(modelAll: event.data);
                  }));
                  break;
                case Status.ERROR:
                  EasyLoading.dismiss();
                  EasyLoading.showError(event.message);
                  _bloce.dispose("all");
                  break;
              }
            });
          },
          child: const ButtonKategori(
            judul: 'Hotel',
            iconnya: CommunityMaterialIcons.home_modern,
          ),
        ),
        GestureDetector(
          onTap: () {
            widget.ytb.pause();
            BlocApp _bloce = BlocApp("all", "Bensin");
            _bloce.allStream.listen((event) {
              switch (event.status) {
                case Status.LOADING:
                  EasyLoading.show(status: 'loading...');
                  break;
                case Status.COMPLETED:
                  EasyLoading.dismiss();
                  _bloce.dispose("all");
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ListDestinasi(modelAll: event.data);
                  }));
                  break;
                case Status.ERROR:
                  EasyLoading.dismiss();
                  EasyLoading.showError(event.message);
                  _bloce.dispose("all");
                  break;
              }
            });
          },
          child: const ButtonKategori(
            judul: 'Pom Bensin',
            iconnya: CommunityMaterialIcons.gas_station,
          ),
        ),
        GestureDetector(
          onTap: () {
            widget.ytb.pause();
            BlocApp _bloce = BlocApp("all", "kesehatan");
            _bloce.allStream.listen((event) {
              switch (event.status) {
                case Status.LOADING:
                  EasyLoading.show(status: 'loading...');
                  break;
                case Status.COMPLETED:
                  EasyLoading.dismiss();
                  _bloce.dispose("all");
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ListDestinasi(modelAll: event.data);
                  }));
                  break;
                case Status.ERROR:
                  EasyLoading.dismiss();
                  EasyLoading.showError(event.message);
                  _bloce.dispose("all");
                  break;
              }
            });
          },
          child: const ButtonKategori(
              judul: 'Kesehatan', iconnya: CommunityMaterialIcons.hospital_box),
        ),
        GestureDetector(
          onTap: () {
            widget.ytb.pause();
            BlocApp _bloce = BlocApp("all", "Event");
            _bloce.allStream.listen((event) {
              switch (event.status) {
                case Status.LOADING:
                  EasyLoading.show(status: 'loading...');
                  break;
                case Status.COMPLETED:
                  EasyLoading.dismiss();
                  _bloce.dispose("all");
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ListDestinasi(modelAll: event.data);
                  }));
                  break;
                case Status.ERROR:
                  EasyLoading.dismiss();
                  EasyLoading.showError(event.message);
                  _bloce.dispose("all");
                  break;
              }
            });
          },
          child: const ButtonKategori(
              judul: 'Event', iconnya: CommunityMaterialIcons.calendar_text),
        ),
        GestureDetector(
          onTap: () {
            widget.ytb.pause();
            BlocApp _bloce = BlocApp("all", "Desa-Wisata");
            _bloce.allStream.listen((event) {
              switch (event.status) {
                case Status.LOADING:
                  EasyLoading.show(status: 'loading...');
                  break;
                case Status.COMPLETED:
                  EasyLoading.dismiss();
                  _bloce.dispose("all");
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ListDestinasi(modelAll: event.data);
                  }));
                  break;
                case Status.ERROR:
                  EasyLoading.dismiss();
                  EasyLoading.showError(event.message);
                  _bloce.dispose("all");
                  break;
              }
            });
          },
          child: const ButtonKategori(
              judul: 'Desa Wisata', iconnya: CommunityMaterialIcons.pine_tree),
        ),
      ],
    );
  }
}

// button kategori =============================================

class ButtonKategori extends StatelessWidget {
  final String judul;
  final IconData iconnya;
  const ButtonKategori({Key? key, required this.judul, required this.iconnya})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.2,
        height: MediaQuery.of(context).size.height * 0.12,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          color: Colors.white.withOpacity(1),
          border: Border.all(),
          boxShadow: const [
            BoxShadow(
                blurRadius: 5,
                spreadRadius: 0.4,
                offset: Offset(0, 3),
                color: Colors.grey)
          ],
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(iconnya, size: 35.0, color: Colors.orange),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.01,
              ),
              Text(
                judul,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
              )
            ],
          ),
        ),
      ),
    );
  }
}

// imageslider ==============================================================
class Imageslider extends StatefulWidget {
  final List<Destinasislider> lisdes;
  final YoutubePlayerController ytb;
  const Imageslider({Key? key, required this.lisdes, required this.ytb})
      : super(key: key);

  @override
  _ImagesliderState createState() => _ImagesliderState();
}

class _ImagesliderState extends State<Imageslider> {
  late ModelDetail modelDetail;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        height: MediaQuery.of(context).size.height * 0.2,
        child: CarouselSlider(
          options: CarouselOptions(
            height: double.infinity,
            viewportFraction: 0.6,
            enlargeCenterPage: true,
            autoPlayInterval: const Duration(seconds: 3),
            autoPlayAnimationDuration: const Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            // enableInfiniteScroll: false
          ),
          items: widget.lisdes.map((i) {
            return Builder(
              builder: (BuildContext context) {
                return GestureDetector(
                  onTap: () {
                    widget.ytb.pause();
                    BlocApp _bloce = BlocApp("detail", i.id);
                    _bloce.detailStream.listen((event) {
                      switch (event.status) {
                        case Status.LOADING:
                          EasyLoading.show(status: 'loading...');
                          break;
                        case Status.COMPLETED:
                          EasyLoading.dismiss();
                          _bloce.dispose("detail");
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return Detail(detail: event.data);
                          }));
                          break;
                        case Status.ERROR:
                          EasyLoading.dismiss();
                          EasyLoading.showError(event.message);
                          _bloce.dispose("detail");
                          break;
                      }
                    });
                  },
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(right: 0.4),
                      decoration: const BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: ItemDestinasi(
                          link: i.link, namades: i.namaDes, alamat: i.alamat)),
                );
              },
            );
          }).toList(),
        ));
  }
}

//=========================================================================

//item image slider destinasi =============================================
class ItemDestinasi extends StatelessWidget {
  final String link;
  final String namades;
  final String alamat;
  const ItemDestinasi(
      {Key? key,
      required this.link,
      required this.namades,
      required this.alamat})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          image: DecorationImage(
              image: NetworkImage(
                link,
              ),
              fit: BoxFit.fill)
          // image: DecorationImage(
          //     image: NetworkImage(
          //       "https://blog.reservasi.com/wp-content/uploads/2018/07/shutterstock_1064654387-1.jpg",
          //     ),
          //     fit: BoxFit.fill)
          ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(left: 5, bottom: 5),
            width: 135.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  namades,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 15.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                Row(
                  children: [
                    Icon(
                      CommunityMaterialIcons.map_marker,
                      size: 10.w,
                      color: Colors.white,
                    ),
                    Expanded(
                      child: Text(
                        alamat,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 10.sp,
                            color: Colors.white),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 5, left: 10, right: 5),
            height: MediaQuery.of(context).size.width * 0.09,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(70),
                  bottomRight: Radius.circular(10)),
              color: Colors.blue,
            ),
            child: Center(
                child: Text(
              'Rekomended',
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontSize: 10.sp,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            )),
          )
        ],
      ),
    );
  }
}

//umkm ===========================================================
class ImageSliderUmkm extends StatefulWidget {
  final List<Umkmslider> lisumkm;
  final YoutubePlayerController ytb;
  const ImageSliderUmkm({Key? key, required this.lisumkm, required this.ytb})
      : super(key: key);

  @override
  _ImageSliderUmkmState createState() => _ImageSliderUmkmState();
}

class _ImageSliderUmkmState extends State<ImageSliderUmkm> {
  late ModelDetail modelDetail;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        height: MediaQuery.of(context).size.height * 0.2,
        child: CarouselSlider(
          options: CarouselOptions(
            height: double.infinity,
            viewportFraction: 0.6,
            enlargeCenterPage: true,
            autoPlayInterval: const Duration(seconds: 3),
            autoPlayAnimationDuration: const Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            // enableInfiniteScroll: false
          ),
          items: widget.lisumkm.map((i) {
            return Builder(
              builder: (BuildContext context) {
                return GestureDetector(
                  onTap: () {
                    widget.ytb.pause();
                    BlocApp _bloce = BlocApp("detail", i.id);
                    _bloce.detailStream.listen((event) {
                      switch (event.status) {
                        case Status.LOADING:
                          EasyLoading.show(status: 'loading...');
                          break;
                        case Status.COMPLETED:
                          EasyLoading.dismiss();
                          _bloce.dispose("detail");
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return Detail(detail: event.data);
                          }));
                          break;
                        case Status.ERROR:
                          EasyLoading.dismiss();
                          EasyLoading.showError(event.message);
                          _bloce.dispose("detail");
                          break;
                      }
                    });
                  },
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(right: 0.4),
                      decoration: const BoxDecoration(
                          color: Color.fromRGBO(255, 165, 0, 1),
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: ItemUmkm(
                        link: i.link,
                        namaUmkm: i.namaumkm,
                        produk: i.produk,
                      )),
                );
              },
            );
          }).toList(),
        ));
  }
}

//item umkm=======================================================
class ItemUmkm extends StatelessWidget {
  final String link;
  final String namaUmkm;
  final String produk;
  const ItemUmkm(
      {Key? key,
      required this.link,
      required this.namaUmkm,
      required this.produk})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Container(
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(10),
                topRight: Radius.circular(90),
                topLeft: Radius.circular(10),
                bottomRight: Radius.circular(10)),
            image: DecorationImage(image: NetworkImage(link), fit: BoxFit.fill)
            // image: DecorationImage(
            //     image: NetworkImage(
            //         "https://blog.reservasi.com/wp-content/uploads/2018/07/shutterstock_1064654387-1.jpg"),
            //     fit: BoxFit.fill)
            ),
        width: 20.w,
        height: 10.h,
        child: Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                SizedBox(
                  child: Icon(
                    CommunityMaterialIcons.star_circle_outline,
                    color: Colors.black,
                  ),
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  namaUmkm,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 15.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                Row(
                  children: [
                    Icon(
                      CommunityMaterialIcons.source_commit_start,
                      size: 10.w,
                      color: Colors.white,
                    ),
                    Expanded(
                      child: Text(
                        produk,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 10.sp,
                            color: Colors.white),
                      ),
                    )
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
