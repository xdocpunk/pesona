import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:klu/data/api_provider.dart';
import 'package:klu/data/bloc.dart';
import 'package:klu/models/models.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:klu/screen/destinasi/list_destinasi.dart';
import 'package:klu/screen/home/wit_home.dart';
import 'package:youtube_parser/youtube_parser.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class Home extends StatefulWidget {
  final ModelHome modelHome;
  const Home({Key? key, required this.modelHome}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late YoutubePlayerController _ytbPlayerController;
  late String? linkYt;

  void pause() {
    _ytbPlayerController.pause();
  }

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    linkYt = getIdFromUrl(widget.modelHome.linkVideo);
    _ytbPlayerController = YoutubePlayerController(
        initialVideoId: linkYt.toString(),
        params: const YoutubePlayerParams(
          showFullscreenButton: true,
          autoPlay: false,
        ));
    _ytbPlayerController.onEnterFullscreen = () {
      setState(() {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight,
        ]);
      });
    };
    _ytbPlayerController.onExitFullscreen = () {
      setState(() {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
        ]);
      });
    };
  }

  @override
  dispose() {
    _ytbPlayerController.close();
    super.dispose();
  }

  @override
  void deactivate() {
    _ytbPlayerController.pause();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //bagian atas 1
              Widgetatas(
                listGambarCarosel: widget.modelHome.listCaroselImage,
                ytb: _ytbPlayerController,
              ),
              //bagian atas 2===================

              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: MediaQuery.of(context).size.height * 0.28,
                      width: double.infinity,
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(),
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.grey,
                                      blurRadius: 5,
                                      spreadRadius: 1,
                                      offset: Offset(0, 3))
                                ]),
                            child: AspectRatio(
                              aspectRatio: 16 / 9,
                              child: _ytbPlayerController != null
                                  ? YoutubePlayerIFrame(
                                      controller: _ytbPlayerController)
                                  : const Center(
                                      child: CircularProgressIndicator()),
                            ),
                          ))),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                      margin: const EdgeInsets.only(left: 15, bottom: 10),
                      child: const Text(
                        'Kategori',
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      )),
                  Container(
                      color: const Color.fromRGBO(12, 110, 25, 1),
                      child: KategorBawah(
                        ytb: _ytbPlayerController,
                      )),
                  Container(
                      margin: const EdgeInsets.only(
                          left: 15, bottom: 10, top: 10, right: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            'Top Destinasi',
                            style: TextStyle(
                                fontSize: 18.sp, fontWeight: FontWeight.bold),
                          ),
                          // InkWell(
                          //   onTap: () {
                          //     pause();
                          //     BlocApp _bloce = BlocApp("all", "wisata");
                          //     _bloce.allStream.listen((event) {
                          //       switch (event.status) {
                          //         case Status.LOADING:
                          //           EasyLoading.show(status: 'loading...');
                          //           break;
                          //         case Status.COMPLETED:
                          //           EasyLoading.dismiss();
                          //           _bloce.dispose("all");
                          //           Navigator.push(context,
                          //               MaterialPageRoute(builder: (context) {
                          //             return ListDestinasi(
                          //                 modelAll: event.data);
                          //           }));
                          //           break;
                          //         case Status.ERROR:
                          //           EasyLoading.dismiss();
                          //           EasyLoading.showError(event.message);
                          //           _bloce.dispose("all");
                          //           break;
                          //       }
                          //     });
                          //   },
                          //   child: Row(
                          //     children: [
                          //       Text(
                          //         'more',
                          //         style: TextStyle(
                          //             fontSize: 12.sp,
                          //             fontWeight: FontWeight.bold),
                          //       ),
                          //       Icon(
                          //         CommunityMaterialIcons
                          //             .arrow_right_thin_circle_outline,
                          //         size: 12.w,
                          //       )
                          //     ],
                          //   ),
                          // ),
                        ],
                      )),
                  Imageslider(
                    lisdes: widget.modelHome.listCaroselDestinasi,
                    ytb: _ytbPlayerController,
                  ),
                ],
              ),
              //===========================================================
              Container(
                  margin: const EdgeInsets.only(
                      left: 15, bottom: 10, top: 10, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        'UMKM',
                        style: TextStyle(
                            fontSize: 18.sp, fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              pause();
                              BlocApp _bloce = BlocApp("all", "umkm");
                              _bloce.allStream.listen((event) {
                                switch (event.status) {
                                  case Status.LOADING:
                                    EasyLoading.show(status: 'loading...');
                                    break;
                                  case Status.COMPLETED:
                                    EasyLoading.dismiss();
                                    _bloce.dispose("all");
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return ListDestinasi(
                                          modelAll: event.data);
                                    }));
                                    break;
                                  case Status.ERROR:
                                    EasyLoading.dismiss();
                                    EasyLoading.showError(event.message);
                                    _bloce.dispose("all");
                                    break;
                                }
                              });
                            },
                            child: Text(
                              'more',
                              style: TextStyle(
                                  fontSize: 12.sp, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Icon(
                            CommunityMaterialIcons
                                .arrow_right_thin_circle_outline,
                            size: 12.w,
                          )
                        ],
                      ),
                    ],
                  )),
              ImageSliderUmkm(
                lisumkm: widget.modelHome.listCaroselUmkm,
                ytb: _ytbPlayerController,
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
