// import 'package:community_material_icon/community_material_icon.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:klu/models/models.dart';
// import 'package:klu/screen/more/wid_more.dart';

// class MoreImages extends StatefulWidget {
//   final ModelMore modelMore;
//   const MoreImages({Key? key, required this.modelMore}) : super(key: key);

//   @override
//   _MoreImagesState createState() => _MoreImagesState();
// }

// class _MoreImagesState extends State<MoreImages> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//         floatingActionButton: FloatingActionButton(
//           backgroundColor: const Color.fromRGBO(12, 110, 25, 1),
//           onPressed: () {
//             Navigator.of(context).pop();
//           },
//           child: const Icon(
//             CommunityMaterialIcons.arrow_left_bold_circle_outline,
//             color: Colors.white,
//           ),
//         ),
//         body: Stack(
//           children: [
//             //background==================
//             Container(
//               width: double.infinity,
//               height: MediaQuery.of(context).size.height * 0.1,
//               color: const Color.fromRGBO(12, 110, 25, 1),
//             ),
//             //content=====================
//             Column(
//               children: [
//                 Container(
//                   margin: EdgeInsets.only(
//                       top: 33.h, left: 20.w, right: 20.w, bottom: 10.h),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     crossAxisAlignment: CrossAxisAlignment.end,
//                     children: [
//                       Text(
//                         "Galeri",
//                         style: TextStyle(
//                             fontFamily: 'Lato',
//                             color: Colors.white,
//                             fontSize: 16.sp,
//                             fontWeight: FontWeight.bold),
//                       ),
//                       Text(
//                         '(' + widget.modelMore.jmlh + ' ditemukan)',
//                         style: TextStyle(
//                             fontFamily: 'Lato',
//                             fontSize: 10.sp,
//                             color: Colors.white,
//                             fontWeight: FontWeight.bold),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Flexible(
//                     child: GridGalery(
//                   listItem: widget.modelMore.listGambar,
//                 ))
//               ],
//             ),
//           ],
//         ));
//   }
// }
