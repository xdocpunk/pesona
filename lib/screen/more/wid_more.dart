import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:klu/screen/detail_destinasi/lihat_gambar.dart';

class GridGalery extends StatefulWidget {
  final List<String> listItem;
  const GridGalery({Key? key, required this.listItem}) : super(key: key);

  @override
  _GridGaleryState createState() => _GridGaleryState();
}

class _GridGaleryState extends State<GridGalery> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 2,
        childAspectRatio: 8 / 5,
      ),
      shrinkWrap: true,
      itemCount: widget.listItem.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        LihatGambarDetail(url: widget.listItem[index])));
          },
          child: ItemGridGalery(linkGambar: widget.listItem[index]),
        );
      },
    );
  }
}

//item grid =========================================
class ItemGridGalery extends StatelessWidget {
  final String linkGambar;
  const ItemGridGalery({Key? key, required this.linkGambar}) : super(key: key);

//   @override
//   _ItemGridGaleryState createState() => _ItemGridGaleryState();
// }

// class _ItemGridGaleryState extends State<ItemGridGalery> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Container(
        padding: const EdgeInsets.all(5),
        margin: EdgeInsets.only(right: 7.w, left: 8.w, bottom: 7),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              topLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          // image: DecorationImage(
          //     image: NetworkImage(
          //         "http://pesona.lombokutarakab.go.id/gambar/" +
          //             widget.linkGambar),
          //     fit: BoxFit.fill)
        ),
        width: 20.w,
        height: 10.h,
        child: Image.network(
          linkGambar,
          alignment: Alignment.center,
          height: double.infinity,
          width: double.infinity,
          loadingBuilder: (BuildContext context, Widget child,
              ImageChunkEvent? loadingProgress) {
            if (loadingProgress == null) return child;
            return Center(
              child: CircularProgressIndicator(
                value: loadingProgress.expectedTotalBytes != null
                    ? loadingProgress.cumulativeBytesLoaded /
                        loadingProgress.expectedTotalBytes!
                    : null,
              ),
            );
          },
        ),
      ),
    );
  }
}
