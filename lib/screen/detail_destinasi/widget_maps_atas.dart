import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:here_sdk/core.dart';
import 'package:here_sdk/mapview.dart';
import 'package:here_sdk/routing.dart';

class Mapnya extends StatefulWidget {
  final double latitude;
  final double longitute;
  const Mapnya({Key? key, required this.latitude, required this.longitute})
      : super(key: key);

  @override
  _MapnyaState createState() => _MapnyaState();
}

class _MapnyaState extends State<Mapnya> {
  late HereMapController controller;
  late MapPolyline mapPolyline;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.3,
        width: MediaQuery.of(context).size.width,
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: HereMap(onMapCreated: onMapCreated));
  }

  Future<void> drawRedDot(HereMapController mapcontroller, int draworder,
      GeoCoordinates geoCoordinates) async {
    ByteData fileData = await rootBundle.load('images/circle.png');
    Uint8List pixelData = fileData.buffer.asUint8List();
    MapImage mapImage =
        MapImage.withPixelDataAndImageFormat(pixelData, ImageFormat.png);
    MapMarker mapMarker = MapMarker(geoCoordinates, mapImage);
    mapMarker.drawOrder = draworder;
    mapcontroller.mapScene.addMapMarker(mapMarker);
  }

  Future<void> drawpin(HereMapController mapcontroller, int draworder,
      GeoCoordinates geoCoordinates) async {
    ByteData fileData = await rootBundle.load('images/poi.png');
    Uint8List pixelData = fileData.buffer.asUint8List();
    MapImage mapImage =
        MapImage.withPixelDataAndImageFormat(pixelData, ImageFormat.png);
    Anchor2D anchor2d = Anchor2D.withHorizontalAndVertical(0.5, 1);
    MapMarker mapMarker =
        MapMarker.withAnchor(geoCoordinates, mapImage, anchor2d);
    mapMarker.drawOrder = draworder;
    mapcontroller.mapScene.addMapMarker(mapMarker);
  }

  Future<void> drawroute(GeoCoordinates star, GeoCoordinates end,
      HereMapController controler) async {
    RoutingEngine routingEngine = RoutingEngine();
    Waypoint startWayPoin = Waypoint.withDefaults(star);
    Waypoint endWayPoin = Waypoint.withDefaults(end);
    List<Waypoint> wayPoints = [startWayPoin, endWayPoin];
    routingEngine.calculatePedestrianRoute(
        wayPoints, PedestrianOptions.withDefaults(), (routingError, routes) {
      if (routingError == null) {
        var route = routes!.first;
        GeoPolyline routeGeoPolyLine = GeoPolyline(route.polyline);
        double dept = 10;
        mapPolyline = MapPolyline(routeGeoPolyLine, dept, Colors.blue);
        controler.mapScene.addMapPolyline(mapPolyline);
      }
    });
  }

  void onMapCreated(HereMapController mapController) {
    controller = mapController;
    mapController.mapScene.loadSceneForMapScheme(MapScheme.hybridDay, (error) {
      if (error != null) {
        return;
      }
    });
    drawRedDot(
        mapController, 0, GeoCoordinates(widget.latitude, widget.longitute));
    drawpin(
        mapController, 1, GeoCoordinates(widget.latitude, widget.longitute));
    double distanceToEarthInMeter = 3000;
    mapController.camera.lookAtPointWithDistance(
        GeoCoordinates(widget.latitude, widget.longitute),
        distanceToEarthInMeter);
  }
}
