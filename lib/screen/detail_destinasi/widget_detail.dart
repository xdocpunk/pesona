import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:klu/screen/detail_destinasi/lihat_gambar.dart';
import 'package:youtube_parser/youtube_parser.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

//youtube playing video ======================================================
class YVideoDetail extends StatefulWidget {
  final String link;
  const YVideoDetail({Key? key, required this.link}) : super(key: key);

  @override
  _YVideoDetailState createState() => _YVideoDetailState();
}

class _YVideoDetailState extends State<YVideoDetail> {
  late YoutubePlayerController _ytbPlayerController;
  late String? linkYt;
  @override
  void initState() {
    super.initState();
    linkYt = getIdFromUrl(widget.link);
    _ytbPlayerController = YoutubePlayerController(
      initialVideoId: linkYt.toString(),
      params: const YoutubePlayerParams(
        showFullscreenButton: true,
        autoPlay: true,
      ),
    );
    _setOrientation([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      setState(() {
        _ytbPlayerController = YoutubePlayerController(
          initialVideoId: widget.link,
          params: const YoutubePlayerParams(
            showFullscreenButton: true,
            autoPlay: true,
          ),
        );
      });
    });
  }

  @override
  void dispose() {
    super.dispose();

    _setOrientation([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    _ytbPlayerController.close();
  }

  _setOrientation(List<DeviceOrientation> orientations) {
    SystemChrome.setPreferredOrientations(orientations);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(),
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          boxShadow: const [
            BoxShadow(
                color: Colors.grey,
                blurRadius: 5,
                spreadRadius: 1,
                offset: Offset(0, 3))
          ]),
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: _ytbPlayerController != null
            ? YoutubePlayerIFrame(controller: _ytbPlayerController)
            : const Center(child: CircularProgressIndicator()),
      ),
    );
  }
}
//galeri============================================================

class Gridgambar extends StatefulWidget {
  final List<String> linkGambar;
  const Gridgambar({Key? key, required this.linkGambar}) : super(key: key);

  @override
  _GridgambarState createState() => _GridgambarState();
}

class _GridgambarState extends State<Gridgambar> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 2,
          childAspectRatio: 8 / 5,
        ),
        shrinkWrap: true,
        itemCount: widget.linkGambar.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          LihatGambarDetail(url: widget.linkGambar[index])));
            },
            child: Isi(url: widget.linkGambar[index]),
          );
        });
  }
}

// Item Galery ====================================

