import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:photo_view/photo_view.dart';

class LihatGambarDetail extends StatefulWidget {
  final String url;
  const LihatGambarDetail({Key? key, required this.url}) : super(key: key);

  @override
  _LihatGambarDetailState createState() => _LihatGambarDetailState();
}

class _LihatGambarDetailState extends State<LihatGambarDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: PhotoView(
        imageProvider: NetworkImage(widget.url),
        // imageProvider: NetworkImage(
        //     "https://blog.reservasi.com/wp-content/uploads/2018/07/shutterstock_1064654387-1.jpg"),
      ),
    ));
  }
}

class Isi extends StatefulWidget {
  final String url;
  const Isi({Key? key, required this.url}) : super(key: key);

  @override
  _IsiState createState() => _IsiState();
}

class _IsiState extends State<Isi> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5.h),
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              topLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          image: DecorationImage(
              image: NetworkImage(widget.url), fit: BoxFit.fill)),
      // image: NetworkImage(
      //     "https://blog.reservasi.com/wp-content/uploads/2018/07/shutterstock_1064654387-1.jpg"),
      // fit: BoxFit.fill)),
    );
  }
}
