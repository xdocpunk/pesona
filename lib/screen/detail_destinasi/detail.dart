import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:klu/models/models.dart';
import 'package:klu/screen/detail_destinasi/widget_maps_atas.dart';
import 'buka_maps.dart';
import 'widget_detail.dart';
import 'package:community_material_icon/community_material_icon.dart';

class Detail extends StatelessWidget {
  final ModelDetail detail;

  const Detail({
    Key? key,
    required this.detail,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(detail.deskr);
    return Scaffold(
        body: Stack(
      children: [
        Mapnya(
          latitude: detail.latitude,
          longitute: detail.longtitude,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.72,
              width: double.infinity,
              padding: EdgeInsets.only(top: 30.h, right: 10.w, left: 10.w),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 3,
                        offset: Offset(1, -3))
                  ]),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 15.w),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Destinasi di Kabupaten Lombok Utara',
                            style: TextStyle(
                                fontFamily: 'Lato',
                                fontSize: 10.sp,
                                color: Colors.pink),
                          ),
                          Text(
                            detail.namaDes,
                            style: TextStyle(
                                fontFamily: 'Lato',
                                fontSize: 30.sp,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10.w,
                    ),
                    Container(
                        height: MediaQuery.of(context).size.height * 0.23,
                        width: double.infinity,
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            child: (detail.linkVideo != "-")
                                ? YVideoDetail(link: detail.linkVideo)
                                : Image.network(
                                    detail.linkGambar[0],
                                    fit: BoxFit.fill,
                                  ))),
                    Container(
                      margin: EdgeInsets.only(left: 10.w, top: 15, right: 15.w),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          (detail.deskr != "<p>-</p>")
                              ? Text(
                                  'Deskripsi',
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontSize: 20.sp,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                )
                              : Container(),
                          SizedBox(
                            height: 5.h,
                          ),
                          (detail.deskr != "<p>-</p>")
                              ? Html(data: detail.deskr)
                              : Container()
                        ],
                      ),
                    ),
                    (detail.alamat != "-")
                        ? Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(top: 20.h),
                            padding: EdgeInsets.symmetric(horizontal: 17.w),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      CommunityMaterialIcons.map_marker,
                                      color: Colors.orange,
                                      size: 15.h,
                                    ),
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Alamat',
                                            style: TextStyle(
                                                fontFamily: 'Lato',
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                          Text(
                                            detail.alamat,
                                            style: TextStyle(
                                                fontFamily: 'Lato',
                                                fontSize: 12.sp,
                                                color: Colors.black),
                                            maxLines: 4,
                                            textAlign: TextAlign.justify,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          )
                        : Container(),
                    (detail.jambuka != "-")
                        ? Container(
                            margin: EdgeInsets.symmetric(vertical: 20.h),
                            padding: EdgeInsets.symmetric(horizontal: 17.w),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.35,
                                      margin: EdgeInsets.only(right: 20.w),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Icon(
                                            CommunityMaterialIcons.av_timer,
                                            color: Colors.orange,
                                            size: 15.h,
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'Jam Buka',
                                                style: TextStyle(
                                                    fontFamily: 'Lato',
                                                    fontSize: 15.sp,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black),
                                              ),
                                              Text(
                                                detail.jambuka,
                                                style: TextStyle(
                                                    fontFamily: 'Lato',
                                                    fontSize: 12.sp,
                                                    color: Colors.black),
                                                maxLines: 4,
                                                textAlign: TextAlign.justify,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                (detail.jamtutup != "-")
                                    ? Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Icon(
                                                CommunityMaterialIcons.av_timer,
                                                color: Colors.orange,
                                                size: 15.h,
                                              ),
                                              Column(
                                                children: [
                                                  Text(
                                                    'Jam Tutup',
                                                    style: TextStyle(
                                                        fontFamily: 'Lato',
                                                        fontSize: 15.sp,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.black),
                                                  ),
                                                  Text(
                                                    detail.jamtutup,
                                                    style: TextStyle(
                                                        fontFamily: 'Lato',
                                                        fontSize: 12.sp,
                                                        color: Colors.black),
                                                    maxLines: 4,
                                                    textAlign:
                                                        TextAlign.justify,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      )
                                    : Container(),
                              ],
                            ),
                          )
                        : Container(),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: 20.h),
                      padding: EdgeInsets.symmetric(horizontal: 17.w),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          (detail.telepon != "-")
                              ? Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.35,
                                  margin: EdgeInsets.only(right: 20.w),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Icon(
                                            CommunityMaterialIcons
                                                .phone_in_talk,
                                            color: Colors.orange,
                                            size: 15.h,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'Telepon',
                                                style: TextStyle(
                                                    fontFamily: 'Lato',
                                                    fontSize: 15.sp,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black),
                                              ),
                                              Text(
                                                detail.telepon,
                                                style: TextStyle(
                                                    fontFamily: 'Lato',
                                                    fontSize: 12.sp,
                                                    color: Colors.black),
                                                maxLines: 4,
                                                textAlign: TextAlign.justify,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              : Container(),
                          (detail.instagrm != "-")
                              ? Expanded(
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Icon(
                                            CommunityMaterialIcons.instagram,
                                            color: Colors.orange,
                                            size: 15.h,
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  'Instagram',
                                                  style: TextStyle(
                                                      fontFamily: 'Lato',
                                                      fontSize: 15.sp,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.black),
                                                ),
                                                Text(
                                                  detail.instagrm,
                                                  style: TextStyle(
                                                      fontFamily: 'Lato',
                                                      fontSize: 12.sp,
                                                      color: Colors.black),
                                                  maxLines: 4,
                                                  textAlign: TextAlign.justify,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(),
                          borderRadius: BorderRadius.all(Radius.circular(10.w)),
                          color: Colors.white,
                          boxShadow: const [
                            BoxShadow(
                                blurRadius: 1,
                                color: Colors.grey,
                                offset: Offset(1, 3))
                          ]),
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height * 0.07,
                      child: ButtonArahkan(
                        lat: detail.latitude,
                        long: detail.longtitude,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10.w, top: 15, right: 15.w),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            'Galeri',
                            style: TextStyle(
                                fontFamily: 'Lato',
                                fontSize: 20.sp,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height * 0.35,
                        child: Gridgambar(linkGambar: detail.linkGambar)),
                    SizedBox(
                      height: 10.h,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        Container(
            padding: EdgeInsets.only(
              top: 28.h,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: Icon(
                      CommunityMaterialIcons.arrow_left,
                      size: 30.w,
                      color: Colors.white,
                    )),
              ],
            )),
      ],
    ));
  }
}
