import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class PreviewImage extends StatefulWidget {
  final String url;
  const PreviewImage({Key? key, required this.url}) : super(key: key);

  @override
  _PreviewImageState createState() => _PreviewImageState();
}

class _PreviewImageState extends State<PreviewImage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: PhotoView(
          imageProvider: NetworkImage(widget.url),
        ),
      ),
    );
  }
}

class ItemGalery extends StatefulWidget {
  final String linkImg;
  const ItemGalery({Key? key, required this.linkImg}) : super(key: key);

  @override
  _ItemGaleryState createState() => _ItemGaleryState();
}

class _ItemGaleryState extends State<ItemGalery> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(5),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(widget.linkImg.toString()),
              fit: BoxFit.fill)),
    );
  }
}
