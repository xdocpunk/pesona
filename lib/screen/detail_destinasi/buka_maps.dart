import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:permission_handler/permission_handler.dart';

class ButtonArahkan extends StatefulWidget {
  final double lat, long;
  const ButtonArahkan({Key? key, required this.lat, required this.long})
      : super(key: key);

  @override
  _ButtonArahkanState createState() => _ButtonArahkanState();
}

class _ButtonArahkanState extends State<ButtonArahkan> {
  void getCurrentLocation() async {
    var permision = await Permission.location.status;
    if (permision.isGranted) {
      var position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.bestForNavigation);
      var lastPosition = await Geolocator.getLastKnownPosition();
      setState(() {
        _launchInBrowser(
            position.latitude, position.longitude, widget.lat, widget.long);
      });
    } else {
      if (await Permission.location.request().isGranted) {
        var position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.bestForNavigation);
        var lastPosition = await Geolocator.getLastKnownPosition();
        setState(() {
          _launchInBrowser(
              position.latitude, position.longitude, widget.lat, widget.long);
        });
      }
    }
  }

  Future<void> _launchInBrowser(double? starLat, double? starlong,
      double? endlat, double? endlong) async {
    String alamat = 'https://www.google.com/maps/dir/' +
        starLat.toString() +
        ',' +
        starlong.toString() +
        '/' +
        endlat.toString() +
        ',' +
        endlong.toString() +
        '/@' +
        starLat.toString() +
        ',' +
        starlong.toString() +
        ',19z/data=!4m2!4m1!3e0';

    if (!await launch(
      alamat,
      forceSafariVC: false,
      forceWebView: false,
      headers: <String, String>{'my_header_key': 'my_header_value'},
    )) {
      throw 'Could not launch $alamat';
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () {
          getCurrentLocation();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              CommunityMaterialIcons.map_marker,
              size: 18.w,
              color: Colors.orange,
            ),
            SizedBox(
              width: 5.w,
            ),
            Text(
              'Arahkan Ke Lokasi',
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontSize: 18.sp,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            )
          ],
        ));
  }
}
