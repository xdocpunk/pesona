import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:klu/models/models.dart';
import 'wid_klu.dart';

class Aboutklu extends StatefulWidget {
  final ModelKlu modelKlu;
  const Aboutklu({Key? key, required this.modelKlu}) : super(key: key);

  @override
  _AboutkluState createState() => _AboutkluState();
}

class _AboutkluState extends State<Aboutklu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //bagian dalam =========
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.35,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("images/bg_tengtang_klu.jpg"),
                      fit: BoxFit.fill)),
              child: Container(
                margin: EdgeInsets.only(right: 10.w, left: 10.w, top: 30.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          CommunityMaterialIcons.arrow_left,
                          size: 30.w,
                          color: Colors.black,
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          height: MediaQuery.of(context).size.height * 0.20,
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(left: 10),
                                child: Image.asset(
                                  'images/logo_klu.png',
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  height:
                                      MediaQuery.of(context).size.height * 0.07,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Kabupaten',
                                    style: TextStyle(
                                        fontFamily: 'Lato',
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                        fontSize: 22.sp),
                                  ),
                                  Text(
                                    'Lombok Utara',
                                    style: TextStyle(
                                        fontFamily: 'Lato',
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 22.sp),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Flexible(
                            child: SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.20,
                                width: MediaQuery.of(context).size.width * 0.45,
                                child: Image.asset(
                                  'images/petaklu.png',
                                  height:
                                      MediaQuery.of(context).size.height * 0.20,
                                  width:
                                      MediaQuery.of(context).size.width * 0.45,
                                  fit: BoxFit.fill,
                                )))
                      ],
                    ),
                  ],
                ),
              ),
            ),

            Container(
              width: double.infinity,
              color: const Color.fromRGBO(12, 110, 25, 1),
              padding: EdgeInsets.only(top: 10.h, bottom: 10.h, left: 20),
              margin: EdgeInsets.only(bottom: 5.h),
              child: Text(
                'Wilayah',
                style: TextStyle(
                    fontFamily: 'Lato',
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 22.sp),
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            SliderKecamatan(
              listKecamatan: widget.modelKlu.listItemKecamatan,
            ),
            Container(
              width: double.infinity,
              color: const Color.fromRGBO(107, 229, 123, 1),
              padding: EdgeInsets.only(top: 10.h, bottom: 10.h, left: 20),
              margin: EdgeInsets.only(top: 20.h),
              child: Text(
                'Deskripsi',
                style: TextStyle(
                    fontFamily: 'Lato',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 22.sp),
              ),
            ),
            Html(data: widget.modelKlu.deskripsi),
            Container(
              margin: EdgeInsets.only(left: 10.w, top: 15, right: 15.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    'Galeri',
                    style: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 20.sp,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ],
              ),
            ),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 20.w),
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.35,
                child: Gridgambar(
                  listGambar: widget.modelKlu.listGambar,
                )),
            SizedBox(
              height: 5.h,
            )
          ],
        ),
      ),
    );
  }
}
