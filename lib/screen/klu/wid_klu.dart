import 'package:carousel_slider/carousel_slider.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:klu/data/api_provider.dart';
import 'package:klu/data/bloc.dart';
import 'package:klu/models/models.dart';
import 'package:klu/screen/detail_destinasi/lihat_gambar.dart';
import 'package:klu/screen/kecamatan/kecamatan.dart';

class SliderKecamatan extends StatefulWidget {
  final List<ItemKecamatanSlider> listKecamatan;
  const SliderKecamatan({Key? key, required this.listKecamatan})
      : super(key: key);

  @override
  _SliderKecamatanState createState() => _SliderKecamatanState();
}

class _SliderKecamatanState extends State<SliderKecamatan> {
  late ModelKecamatan modelKecamatan;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        height: MediaQuery.of(context).size.height * 0.2,
        child: CarouselSlider(
          options: CarouselOptions(
            height: double.infinity,
            viewportFraction: 0.6,
            enlargeCenterPage: true,
            autoPlayInterval: const Duration(seconds: 3),
            autoPlayAnimationDuration: const Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            // enableInfiniteScroll: false
          ),
          items: widget.listKecamatan.map((i) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(right: 0.4),
                    decoration: const BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: GestureDetector(
                      onTap: () {
                        BlocApp _bloce = BlocApp("kecamatan", i.id);
                        _bloce.kecamatanStream.listen((event) {
                          switch (event.status) {
                            case Status.LOADING:
                              EasyLoading.show(status: 'loading...');
                              break;
                            case Status.COMPLETED:
                              EasyLoading.dismiss();
                              _bloce.dispose("kecamatan");
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return Kecamatan(modelKecamatana: event.data);
                              }));
                              break;
                            case Status.ERROR:
                              EasyLoading.dismiss();
                              EasyLoading.showError(event.message);
                              _bloce.dispose("kecamatan");
                              break;
                          }
                        });
                      },
                      child: ItemKecamatan(
                          link: i.link, namades: i.nama, alamat: i.alamat),
                    ));
              },
            );
          }).toList(),
        ));
  }
}

//=========================================================================

//item image slider destinasi =============================================
class ItemKecamatan extends StatelessWidget {
  final String link;
  final String namades;
  final String alamat;
  const ItemKecamatan(
      {Key? key,
      required this.link,
      required this.namades,
      required this.alamat})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          image: DecorationImage(image: AssetImage(link), fit: BoxFit.fill)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(left: 5, bottom: 5),
            width: 190.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Kecamatan " + namades,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 15.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      CommunityMaterialIcons.map_marker_circle,
                      size: 10.w,
                      color: Colors.white,
                    ),
                    Text(
                      alamat,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 4,
                      style: TextStyle(
                          fontFamily: 'Lato',
                          fontSize: 10.sp,
                          color: Colors.white),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
//===============================================
//galeri============================================================

class Gridgambar extends StatelessWidget {
  final List<String> listGambar;
  const Gridgambar({Key? key, required this.listGambar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 2,
          childAspectRatio: 8 / 5,
        ),
        shrinkWrap: true,
        itemCount: listGambar.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          LihatGambarDetail(url: listGambar[index])));
            },
            child: Isi(url: listGambar[index]),
          );
        });
  }
}

// Item Galery ====================================
class ItemGalery extends StatelessWidget {
  final String linkImg;
  const ItemGalery({Key? key, required this.linkImg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(5),
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          image:
              DecorationImage(image: NetworkImage(linkImg), fit: BoxFit.fill)),
    );
  }
}
