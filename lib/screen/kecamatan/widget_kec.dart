import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

//item image slider destinasi =============================================
class ItemKecamatan extends StatefulWidget {
  final String link;
  final String namades;
  final String alamat;
  const ItemKecamatan(
      {Key? key,
      required this.link,
      required this.namades,
      required this.alamat})
      : super(key: key);

  @override
  _ItemKecamatanState createState() => _ItemKecamatanState();
}

class _ItemKecamatanState extends State<ItemKecamatan> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          image: DecorationImage(
              image: AssetImage(widget.link), fit: BoxFit.fill)),
      // image: NetworkImage(
      //     "https://blog.reservasi.com/wp-content/uploads/2018/07/shutterstock_1064654387-1.jpg"),
      // fit: BoxFit.fill)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(left: 5, bottom: 5),
            width: 190.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.namades,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 15.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      CommunityMaterialIcons.map_marker_circle,
                      size: 10.w,
                      color: Colors.white,
                    ),
                    Text(
                      widget.alamat,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 4,
                      style: TextStyle(
                          fontFamily: 'Lato',
                          fontSize: 10.sp,
                          color: Colors.white),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
//===============================================

//item listview==================================
class ItemListDest extends StatefulWidget {
  final String link;
  final String namaDes;
  final String ket;
  const ItemListDest(
      {Key? key, required this.link, required this.namaDes, required this.ket})
      : super(key: key);

  @override
  _ItemListDestState createState() => _ItemListDestState();
}

class _ItemListDestState extends State<ItemListDest> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10.w, right: 10.w, bottom: 5.h),
      padding: EdgeInsets.all(5.w),
      decoration: const BoxDecoration(
          // border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                color: Colors.grey,
                blurRadius: 5,
                spreadRadius: 1,
                offset: Offset(0, 3))
          ],
          color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 90.w,
            height: 90.w,
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(5)),
                image: DecorationImage(
                    image: NetworkImage(widget.link), fit: BoxFit.fill)),
            // image: NetworkImage(
            //     "https://blog.reservasi.com/wp-content/uploads/2018/07/shutterstock_1064654387-1.jpg"),
            // fit: BoxFit.fill)
            // ),
          ),
          SizedBox(
            width: 10.w,
          ),
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 5.h,
                ),
                Text(
                  widget.namaDes,
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 20.sp,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      overflow: TextOverflow.ellipsis),
                ),
                SizedBox(
                  height: 5.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      CommunityMaterialIcons.power_off,
                      color: Colors.orange,
                      size: 10.h,
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    Flexible(
                        child: SizedBox(
                            height: MediaQuery.of(context).size.height * 0.08,
                            child: Text(
                              stripHtmlIfNeeded(widget.ket),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                              style: TextStyle(
                                  fontFamily: 'Lato',
                                  fontSize: 12.sp,
                                  color: Colors.black),
                            ))),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

String stripHtmlIfNeeded(String text) {
  return text.replaceAll(RegExp(r'<[^>]*>|&[^;]+;'), ' ');
}
