import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:klu/data/api_provider.dart';
import 'package:klu/data/bloc.dart';
import 'package:klu/models/models.dart';
import 'package:klu/screen/detail_destinasi/detail.dart';
import 'package:klu/screen/kecamatan/widget_kec.dart';

class Kecamatan extends StatefulWidget {
  final ModelKecamatan modelKecamatana;
  const Kecamatan({Key? key, required this.modelKecamatana}) : super(key: key);

  @override
  _KecamatanState createState() => _KecamatanState();
}

class _KecamatanState extends State<Kecamatan> {
  late ModelKecamatan modelKecamatan;
  late String peta;
  late List<ItemListViewKecamatan> listItemnya;
  late List<ItemKecamatanSlider> listItemSlider;
  late int aktif = 0;
  @override
  void initState() {
    super.initState();
    modelKecamatan = widget.modelKecamatana;
    listItemnya = modelKecamatan.listViewKecamatan;
    listItemSlider = modelKecamatan.listKecamatanSlider;
    switch (modelKecamatan.nama) {
      case "bayan":
        peta = "images/bayan.png";
        break;
      case "gangga":
        peta = "images/gangga.png";
        break;
      case "kayangan":
        peta = "images/kayangan.png";
        break;
      case "pemenang":
        peta = "images/pemenang.png";
        break;
      case "tanjung":
        peta = "images/tanjung.png";
        break;
      default:
        peta = "images/petaklu.png";
    }
  }

  @override
  Widget build(BuildContext context) {
    Map<String, IconData> iconMapping = {
      'Wisata': CommunityMaterialIcons.map_marker_distance,
      'Hotel': CommunityMaterialIcons.home_modern,
      'Bensin': CommunityMaterialIcons.gas_station,
      'Kesehatan': CommunityMaterialIcons.hospital_box,
      'UMKM': CommunityMaterialIcons.circle_box_outline,
      'Event': CommunityMaterialIcons.calendar_text,
      'Makanan': CommunityMaterialIcons.food,
      'DesaWisata': CommunityMaterialIcons.pine_tree,
    };
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.45,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(modelKecamatan.linkGambar),
                    // image: NetworkImage(
                    //     "https://blog.reservasi.com/wp-content/uploads/2018/07/shutterstock_1064654387-1.jpg"),
                    fit: BoxFit.fill)),
            child: Container(
              margin: EdgeInsets.only(right: 10.w, left: 10.w, top: 30.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        CommunityMaterialIcons.arrow_left,
                        size: 30.w,
                        color: Colors.black,
                      )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: const EdgeInsets.all(10),
                        height: MediaQuery.of(context).size.height * 0.20,
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 10),
                              child: Image.asset(
                                'images/logo_klu.png',
                                width: MediaQuery.of(context).size.width * 0.1,
                                height:
                                    MediaQuery.of(context).size.height * 0.07,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Kecamatan',
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontSize: 19.sp),
                                ),
                                Text(
                                  modelKecamatan.nama,
                                  style: TextStyle(
                                      fontFamily: 'Lato',
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.sp),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Flexible(
                          child: SizedBox(
                              height: MediaQuery.of(context).size.height * 0.20,
                              width: MediaQuery.of(context).size.width * 0.45,
                              child: Image.asset(
                                peta,
                                height:
                                    MediaQuery.of(context).size.height * 0.20,
                                width: MediaQuery.of(context).size.width * 0.45,
                                fit: BoxFit.fill,
                              )))
                    ],
                  ),
                ],
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                padding: EdgeInsets.only(top: 20.w),
                height: MediaQuery.of(context).size.height * 0.68,
                width: double.infinity,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20)),
                    color: Colors.white),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                            right: 15.w, left: 15.w, bottom: 10.h),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Destinasi di Kabupaten Lombok Utara',
                              style: TextStyle(
                                  fontFamily: 'Lato',
                                  fontSize: 12.sp,
                                  color: Colors.pink),
                            ),
                            Text(
                              'Kategori',
                              style: TextStyle(
                                  fontFamily: 'Lato',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25.sp,
                                  overflow: TextOverflow.ellipsis),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 15.w, right: 15.w, bottom: 10.h),
                        height: 50.h,
                        width: double.infinity,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                BlocApp _bloce = BlocApp(
                                    "deskecamatan",
                                    modelKecamatan.listKategori[index].kode +
                                        "-" +
                                        modelKecamatan.nama);
                                _bloce.deskecamatanStream.listen((event) {
                                  switch (event.status) {
                                    case Status.LOADING:
                                      EasyLoading.show(status: 'loading...');
                                      break;
                                    case Status.COMPLETED:
                                      EasyLoading.dismiss();
                                      _bloce.dispose("deskecamatan");
                                      listItemnya = event.data.listdestinasi;
                                      aktif = index;
                                      EasyLoading.dismiss();
                                      setState(() {});
                                      break;
                                    case Status.ERROR:
                                      EasyLoading.dismiss();
                                      EasyLoading.showError(event.message);
                                      _bloce.dispose("deskecamatan");
                                      break;
                                  }
                                });
                              },
                              child: Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(10.h),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                      color: (index == aktif)
                                          ? Colors.grey
                                          : Colors.white,
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Colors.grey,
                                            blurRadius: 1,
                                            spreadRadius: 1,
                                            offset: Offset(0, 3))
                                      ],
                                    ),
                                    child: Row(
                                      children: [
                                        Icon(
                                          iconMapping[modelKecamatan
                                              .listKategori[index].nama],
                                          // CommunityMaterialIcons.access_point,
                                          size: 15.w,
                                          color: (index == aktif)
                                              ? Colors.white
                                              : Colors.orange,
                                        ),
                                        SizedBox(
                                          width: 5.w,
                                        ),
                                        Text(
                                          modelKecamatan
                                              .listKategori[index].nama,
                                          style: TextStyle(
                                              color: (index == aktif)
                                                  ? Colors.white
                                                  : Colors.black,
                                              fontFamily: "Lato"),
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.w,
                                  ),
                                ],
                              ),
                            );
                          },
                          itemCount: modelKecamatan.listKategori.length,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 5.w, right: 5.w, bottom: 15.h),
                        height: MediaQuery.of(context).size.height * 0.4,
                        child: ListView.builder(
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                BlocApp _bloce =
                                    BlocApp("detail", listItemnya[index].id);
                                _bloce.detailStream.listen((event) {
                                  switch (event.status) {
                                    case Status.LOADING:
                                      EasyLoading.show(status: 'loading...');
                                      break;
                                    case Status.COMPLETED:
                                      EasyLoading.dismiss();
                                      _bloce.dispose("detail");
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return Detail(detail: event.data);
                                      }));
                                      break;
                                    case Status.ERROR:
                                      EasyLoading.dismiss();
                                      EasyLoading.showError(event.message);
                                      _bloce.dispose("detail");
                                      break;
                                  }
                                });
                              },
                              child: Column(
                                children: [
                                  //=============
                                  ItemListDest(
                                    namaDes: listItemnya[index].nama,
                                    ket: listItemnya[index].deskripsi,
                                    link: listItemnya[index].link,
                                  ),
                                  //=============
                                  SizedBox(
                                    height: 5.h,
                                  )
                                ],
                              ),
                            );
                          },
                          itemCount: listItemnya.length,
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        color: const Color.fromRGBO(12, 110, 25, 1),
                        padding:
                            EdgeInsets.only(top: 10.h, bottom: 10.h, left: 20),
                        margin: EdgeInsets.only(bottom: 15.h),
                        child: Text(
                          'Terkait',
                          style: TextStyle(
                              fontFamily: 'Lato',
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 22.sp),
                        ),
                      ),
                      SizedBox(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height * 0.2,
                          child: CarouselSlider(
                            options: CarouselOptions(
                              height: double.infinity,
                              viewportFraction: 0.6,
                              enlargeCenterPage: true,
                              autoPlayInterval: const Duration(seconds: 3),
                              autoPlayAnimationDuration:
                                  const Duration(milliseconds: 800),
                              autoPlayCurve: Curves.fastOutSlowIn,
                              // enableInfiniteScroll: false
                            ),
                            items: listItemSlider.map((i) {
                              return Builder(
                                builder: (BuildContext context) {
                                  return GestureDetector(
                                    onTap: () {
                                      BlocApp _bloce =
                                          BlocApp("kecamatan", i.id);
                                      _bloce.kecamatanStream.listen((event) {
                                        switch (event.status) {
                                          case Status.LOADING:
                                            EasyLoading.show(
                                                status: 'loading...');
                                            break;
                                          case Status.COMPLETED:
                                            EasyLoading.dismiss();
                                            _bloce.dispose("kecamatan");
                                            modelKecamatan = event.data;
                                            listItemnya = modelKecamatan
                                                .listViewKecamatan;
                                            listItemSlider = modelKecamatan
                                                .listKecamatanSlider;
                                            switch (modelKecamatan.nama) {
                                              case "bayan":
                                                peta = "images/bayan.png";
                                                break;
                                              case "gangga":
                                                peta = "images/gangga.png";
                                                break;
                                              case "kayangan":
                                                peta = "images/kayangan.png";
                                                break;
                                              case "pemenang":
                                                peta = "images/pemenang.png";
                                                break;
                                              case "tanjung":
                                                peta = "images/tanjung.png";
                                                break;
                                              default:
                                                peta = "images/petaklu.png";
                                            }
                                            setState(() {});
                                            break;
                                          case Status.ERROR:
                                            EasyLoading.dismiss();
                                            EasyLoading.showError(
                                                event.message);
                                            _bloce.dispose("kecamatan");
                                            break;
                                        }
                                      });
                                    },
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        margin:
                                            const EdgeInsets.only(right: 0.4),
                                        decoration: const BoxDecoration(
                                            color: Colors.amber,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: ItemKecamatan(
                                            link: i.link,
                                            namades: i.nama,
                                            alamat: i.alamat)),
                                  );
                                },
                              );
                            }).toList(),
                          )),
                      SizedBox(
                        height: 17.h,
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
