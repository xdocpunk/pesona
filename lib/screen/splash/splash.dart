import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('images/bgSplash.jpg'), fit: BoxFit.fill)),
          child: Stack(
            children: [
              Container(
                  margin: EdgeInsets.only(left: 15.h, top: 30.h),
                  child: Row(
                    children: [
                      Image.asset(
                        'images/logo_klu.png',
                        width: MediaQuery.of(context).size.width * 0.1,
                        height: MediaQuery.of(context).size.width * 0.1,
                      ),
                      Image.asset(
                        'images/logo_kominfo.png',
                        width: MediaQuery.of(context).size.width * 0.1,
                        height: MediaQuery.of(context).size.width * 0.1,
                      )
                    ],
                  )),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.3,
                      height: MediaQuery.of(context).size.width * 0.3,
                      child: Image.asset('images/iconnya.png'),
                    ),
                    Text(
                      'Pesona Lombok Utara',
                      style: TextStyle(
                          fontFamily: 'Kausan',
                          color: Colors.white,
                          fontSize: 30.sp,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Explore the Beauty of North Lombok Destination',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Handlee',
                        color: Colors.white,
                        fontSize: 15.sp,
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.3,
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(
                          bottom: MediaQuery.of(context).size.height * 0.1),
                      child: Image.asset(
                        "images/loading.gif",
                        height: MediaQuery.of(context).size.width * 0.1,
                        width: MediaQuery.of(context).size.width * 0.1,
                      ),
                    ),
                  )
                ],
              )
            ],
          )),
    );
  }
}
