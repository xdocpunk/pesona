import 'package:klu/models/models.dart';

import 'api_provider.dart';

class RepositoryApp {
  ApiProvider _provider = ApiProvider();

  Future<ModelHome> fetchHomeData() async {
    final response = await _provider.get("home");
    return ModelHome.create(response);
  }

  Future<ModelAll> fetchAllDestinasiData(String key) async {
    final response = await _provider.get("allDesByCat?alias=" + key);
    return ModelAll.create(response);
  }

  Future<ModelDetail> fetchDetailData(String key) async {
    final response = await _provider.get("detail?id=" + key);
    return ModelDetail.create(response);
  }

  Future<ModelSearch> fetchPencarianData(String key) async {
    final response = await _provider.get("search?key=" + key);
    return ModelSearch.create(response);
  }

  Future<ModelKlu> fetchKluData() async {
    final response = await _provider.get("tentang-klu");
    return ModelKlu.create(response);
  }

  Future<ModelKecamatan> fetchKecamatanData(String key) async {
    final response = await _provider.get("kecamatan?kecamatan=" + key);
    return ModelKecamatan.create(response);
  }

  Future<ListViewKecamatan> fetchDestinasiKecamatanData(String key) async {
    var k = key.split("-");
    final response = await _provider.get("detailkecByCat?kecByCat=" +
        k[0].toString() +
        "&district=" +
        k[1].toString());
    return ListViewKecamatan.create(response);
  }
}
