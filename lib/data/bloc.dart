import 'dart:async';

import 'package:klu/data/api_provider.dart';
import 'package:klu/models/models.dart';

import 'repository.dart';

class BlocApp {
  late RepositoryApp _repository;
  //home================================================================
  late StreamController<Response<ModelHome>> _homeController;
  StreamSink<Response<ModelHome>> get homeSink => _homeController.sink;
  Stream<Response<ModelHome>> get homeStream => _homeController.stream;
  //====================================================================
  //modelall================================================================
  late StreamController<Response<ModelAll>> _allController;
  StreamSink<Response<ModelAll>> get allSink => _allController.sink;
  Stream<Response<ModelAll>> get allStream => _allController.stream;
  //====================================================================
  //detail================================================================
  late StreamController<Response<ModelDetail>> _detailController;
  StreamSink<Response<ModelDetail>> get detailSink => _detailController.sink;
  Stream<Response<ModelDetail>> get detailStream => _detailController.stream;
  //====================================================================
  //cari================================================================
  late StreamController<Response<ModelSearch>> _cariController;
  StreamSink<Response<ModelSearch>> get cariSink => _cariController.sink;
  Stream<Response<ModelSearch>> get cariStream => _cariController.stream;
  //====================================================================
  //klu================================================================
  late StreamController<Response<ModelKlu>> _kluController;
  StreamSink<Response<ModelKlu>> get kluSink => _kluController.sink;
  Stream<Response<ModelKlu>> get kluStream => _kluController.stream;
  //====================================================================
  //kecamatan================================================================
  late StreamController<Response<ModelKecamatan>> _kecamatanController;
  StreamSink<Response<ModelKecamatan>> get kecamatanSink =>
      _kecamatanController.sink;
  Stream<Response<ModelKecamatan>> get kecamatanStream =>
      _kecamatanController.stream;
  //====================================================================
//Destinasikecamatan================================================================
  late StreamController<Response<ListViewKecamatan>> _deskecamatanController;
  StreamSink<Response<ListViewKecamatan>> get deskecamatanSink =>
      _deskecamatanController.sink;
  Stream<Response<ListViewKecamatan>> get deskecamatanStream =>
      _deskecamatanController.stream;
  //====================================================================

  BlocApp(String page, String key) {
    _repository = RepositoryApp();
    switch (page) {
      case "home":
        _homeController = StreamController<Response<ModelHome>>();
        fetchData(page);
        break;
      case "all":
        _allController = StreamController<Response<ModelAll>>();
        fetchDataWithParam(page, key);
        break;
      case "detail":
        _detailController = StreamController<Response<ModelDetail>>();
        fetchDataWithParam(page, key);
        break;
      case "cari":
        _cariController = StreamController<Response<ModelSearch>>();
        fetchDataWithParam(page, key);
        break;
      case "klu":
        _kluController = StreamController<Response<ModelKlu>>();
        fetchData(page);
        break;
      case "kecamatan":
        _kecamatanController = StreamController<Response<ModelKecamatan>>();
        fetchDataWithParam(page, key);
        break;
      case "deskecamatan":
        _deskecamatanController =
            StreamController<Response<ListViewKecamatan>>();
        fetchDataWithParam(page, key);
        break;
    }
  }

  fetchData(String page) async {
    switch (page) {
      case "home":
        homeSink.add(Response.loading('Loading ....!'));
        try {
          ModelHome chuckCats = await _repository.fetchHomeData();
          homeSink.add(Response.completed(chuckCats));
        } catch (e) {
          homeSink.add(Response.error(e.toString()));
          print(e);
        }
        break;
      case "klu":
        kluSink.add(Response.loading('Loading ....!'));
        try {
          ModelKlu chuckCats = await _repository.fetchKluData();
          kluSink.add(Response.completed(chuckCats));
        } catch (e) {
          kluSink.add(Response.error(e.toString()));
          print(e);
        }
        break;
    }
  }

  fetchDataWithParam(String page, String key) async {
    switch (page) {
      case "all":
        allSink.add(Response.loading('Loading ....!'));
        try {
          ModelAll chuckCats = await _repository.fetchAllDestinasiData(key);
          allSink.add(Response.completed(chuckCats));
        } catch (e) {
          allSink.add(Response.error(e.toString()));
          print(e);
        }
        break;
      case "detail":
        detailSink.add(Response.loading('Loading ....!'));
        try {
          ModelDetail chuckCats = await _repository.fetchDetailData(key);
          detailSink.add(Response.completed(chuckCats));
        } catch (e) {
          detailSink.add(Response.error(e.toString()));
          print(e);
        }
        break;
      case "cari":
        cariSink.add(Response.loading('Loading ....!'));
        try {
          ModelSearch chuckCats = await _repository.fetchPencarianData(key);
          cariSink.add(Response.completed(chuckCats));
        } catch (e) {
          cariSink.add(Response.error(e.toString()));
          print(e);
        }
        break;
      case "kecamatan":
        kecamatanSink.add(Response.loading('Loading ....!'));
        try {
          ModelKecamatan chuckCats = await _repository.fetchKecamatanData(key);
          kecamatanSink.add(Response.completed(chuckCats));
        } catch (e) {
          kecamatanSink.add(Response.error(e.toString()));
          print(e);
        }
        break;
      case "deskecamatan":
        deskecamatanSink.add(Response.loading('Loading ....!'));
        try {
          ListViewKecamatan chuckCats =
              await _repository.fetchDestinasiKecamatanData(key);
          deskecamatanSink.add(Response.completed(chuckCats));
        } catch (e) {
          deskecamatanSink.add(Response.error(e.toString()));
          print(e);
        }
        break;
    }
  }

  dispose(String page) {
    switch (page) {
      case "home":
        _homeController.close();
        break;
      case "all":
        _allController.close();
        break;
      case "detail":
        _detailController.close();
        break;
      case "cari":
        _cariController.close();
        break;
      case "klu":
        _kluController.close();
        break;
      case "kecamatan":
        _kecamatanController.close();
        break;
      case "deskecamatan":
        _deskecamatanController.close();
        break;
    }
  }
}
