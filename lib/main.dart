import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:here_sdk/core.dart';
import 'package:klu/data/api_provider.dart';
import 'package:klu/data/bloc.dart';
import 'package:klu/screen/splash/splash.dart';
import 'screen/home/home.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  SdkContext.init(IsolateOrigin.main);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: () => MaterialApp(
        title: 'Pesona KLU',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(title: 'Flutter Demo Home Page'),
        builder: EasyLoading.init(),
      ),
      designSize: const Size(360, 640),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    BlocApp _bloce = BlocApp("home", "");
    _bloce.homeStream.listen((event) {
      switch (event.status) {
        case Status.LOADING:
          break;
        case Status.COMPLETED:
          EasyLoading.dismiss();
          _bloce.dispose("home");
          Timer(
              const Duration(seconds: 1),
              () => Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Home(
                            modelHome: event.data,
                          ))));
          break;
        case Status.ERROR:
          EasyLoading.showError(event.message);
          _bloce.dispose("home");
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (_, orientation) {
      if (orientation == Orientation.portrait) {
        return Splash();
      } else {
        return Container();
      } // else show the landscape one
    });
  }
}
